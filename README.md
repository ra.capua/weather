Weather app that shows updated weather data for up to five different cities.

### Requirements

  - [NodeJS v14](https://nodejs.org/en/download/current/)

### How to run

Open a terminal in the repo folder and run `npm install`, followed afterwards by `npm start`. You can view the app through [http://localhost:3000](http://localhost:3000) (a browser window should automatically open and navigate to said location).

### How to use

Type the city in the provided form below, then select how often you'd like it to update, whether it be every minute, 5 minutes or 10 minutes. Afterwards, click "Add" to add the location to the list. You can only have a maximum of 5 locations.