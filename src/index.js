import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import Axios from 'axios';

function Tile(props) {
  // State variables
  const [temperature, setTemperature] = useState(0);
  const [weatherName, setWeatherName] = useState(0);

  const updateTile = 
    () => {
      // Call the OpenWeather API through a CORS proxy to get through CORS protection
      const corsProxy = 'https://cors-anywhere.herokuapp.com/';
      const url = `https://api.openweathermap.org/data/2.5/weather?q=${props.cityName}&units=metric&appid=3825fe7800a0ec7ff1fde6d1c2854ec5`;

      // HTTP GET request using Axios
      Axios.get(corsProxy + url)
        .then(res => 
          {
            setWeatherName(res.data.weather[0].main);
            setTemperature(res.data.main.temp);
          })
        .catch(err => {
            setWeatherName('No data');
            setTemperature(0);
          });
    };

  // On-load (aka componentDidMount) code
  useEffect(() => {
    // Update tile once, then set up to repeat at the specified interval
    // props.interval is in seconds so multiply it by 1000
    updateTile();
    setInterval(updateTile, props.interval * 1000);
  }, []);

  return (
    <div className="tile">
      <div className="name">
        <div>{props.cityName}</div>
        <button className="remove-tile btn btn-danger" onClick={props.removeTile}>X</button>
      </div>
      <img src={getWeatherIcon(weatherName)} alt={weatherName}/>
      <div className="conditions">
        <div className="temperature">{temperature}C</div>
        <div className="weather-name">{weatherName}</div>
      </div>
    </div>
  );
}

function Weather() {
  // State variables
  // id is for identifying 
  const [tiles, setTiles] = useState([]);
  const [inputCity, setInputCity] = useState('');
  const [intervalId, setIntervalId] = useState(1);
  const [id, setId] = useState(1);
  const [tileCount, setTileCount] = useState(0);
  
  // Handlers for controlled components
  const handleCityChange = event => setInputCity(event.target.value);
  const handleIntervalIdChange = event => setIntervalId(event.target.value);

  const tileLimit = 5;

  // Handler for adding a weather tile
  const handleSubmit = 
    event => {
      event.preventDefault();

      // Validation
      // Prevents user from adding tiles with blank city names
      // and enforces the tile limit
      if (!inputCity || inputCity === '' || tileCount === tileLimit) {
        return;
      }

      let newTile = {
        id: id,
        cityName: inputCity,
        interval: convertInterval(intervalId)
      }

      setTiles(tiles.concat([newTile]));

      // Reset form fields
      setInputCity('');
      setIntervalId(1);

      // Update tile ID and tile count
      setId(id + 1);
      setTileCount(tileCount + 1);
    };

  // Handler for removing a weather tile
  const handleRemove =
    id => {
      const tileToRemove = tiles.find(t => t.id === id);
      const tileIndex = tiles.indexOf(tileToRemove);
      const newTiles = [...tiles];
      newTiles.splice(tileIndex, 1);
      setTiles(newTiles);
      setTileCount(tileCount - 1);
    }

  // Prepares the list of tiles for rendering
  const tileList = tiles.map((tile) => 
    <Tile 
      key={tile.id} 
      cityName={tile.cityName} 
      interval={tile.interval}
      removeTile={() => handleRemove(tile.id)}
    />
  );

  // Rendering
  return (
    <div className="weather-container">
      <div className="tiles">
        {tileList}
      </div>
      <form>
          Enter City
          <input className="form-control" type="text" name="name" value={inputCity} onChange={handleCityChange} />
          <select className="form-control" name="interval" value={intervalId} onChange={handleIntervalIdChange}>
            <option value="1">1 minute</option>
            <option value="2">5 minutes</option>
            <option value="3">10 minutes</option>
          </select>
          <button className="btn btn-primary" type="submit" onClick={handleSubmit}>Add</button>
      </form>
    </div>
  );
}

// ========================================
  
ReactDOM.render(
  <Weather />,
  document.getElementById('root')
);

// Helper functions

// Returns the URL of the image the corresponds to the provided weather name
function getWeatherIcon(weatherName) {
  let weatherConditions = 
    ['thunderstorm', 'drizzle', 'rain', 'snow',
    'haze', 'dust', 'fog', 'sand', 'ash', 'squall', 
    'tornado', 'clear', 'clouds'];

  if (!weatherName) return 'logo512.png';
  if (weatherConditions.indexOf((weatherName.toLowerCase())) !== -1) {
    return `weathericons/${weatherName.toLowerCase()}.png`;
  } else {
    return 'logo512.png';
  }
}

// Converts the id given by the interval selection box to an actual time (in seconds)
function convertInterval(id) {
  switch (id) {
    case 1:
      return 60; // Option 1: 1 minute
    case 2:
      return 300; // Option 2: 5 minutes
    case 3:
      return 600; // Option 3: 10 minutes
    default:
      return 3600; // Default: 1 hour
  }
}